package com.tri.itebookwithretrofit;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tri.itebookwithretrofit.retrofit.EbookService;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit.Call;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.txt_search) EditText txtSearch;
    @Bind(R.id.btn_search) Button btnSearch;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mProgressDialog = new ProgressDialog(this);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_search)
    public void cariData(){
        asynRequest(txtSearch.getText().toString());
    }

    private void asynRequest(String keyword){
        mProgressDialog.setMessage("Pencarian...");
        mProgressDialog.setCancelable(false);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://it-ebooks-api.info/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        EbookService ebookService = retrofit.create(EbookService.class);
        Call<com.tri.itebookwithretrofit.model.Response> call = ebookService.buku(keyword,1);
        call.enqueue(new Callback<com.tri.itebookwithretrofit.model.Response>() {
            @Override
            public void onResponse(Response<com.tri.itebookwithretrofit.model.Response> response, Retrofit retrofit) {
                mProgressDialog.dismiss();
                if(!response.body().getTotal().equals("0")){
                    for(int i =0; i<response.body().getBooks().size(); i++){
                        Log.d("onResponse ==>: ",response.body().getBooks().get(i).getTitle());
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Data tidak ditemukan",Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Throwable t) {
                mProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(),"Ups, Something wrong",Toast.LENGTH_SHORT).show();
                Log.d("Error response",t.toString());
            }
        });
    }
}
