package com.tri.itebookwithretrofit.model;

import java.util.List;

/**
 * Created by tri on 12/20/15.
 */
public class Response {

    /**
     * Error : 0
     * Time : 0.0111
     * Total : 179
     * Page : 1
     * Books : [{"ID":1548294494,"Title":"Linux System Administration","SubTitle":"Solve Real-life Linux Problems Quickly","Description":"If you're an experienced system administrator looking to acquire Linux skills, or a seasoned Linux user facing a new challenge, Linux System Administration offers practical knowledge for managing a complete range of Linux systems and servers. The boo ...","Image":"http://s.it-ebooks-api.info/3/linux_system_administration.jpg","isbn":"9780596009526"},{"ID":674661311,"Title":"Linux Kernel Development, 3rd Edition","SubTitle":"A thorough guide to the design and implementation of the Linux kernel","Description":"Linux Kernel Development details the design and implementation of the Linux kernel, presenting the content in a manner that is beneficial to those writing and developing kernel code, as well as to programmers seeking to better understand the operatin ...","Image":"http://s.it-ebooks-api.info/10/linux_kernel_development_3rd_edition.jpg","isbn":"9780672329463"},{"ID":1311043291,"Title":"Hacking Exposed Linux, 3rd Edition","SubTitle":"Linux Security Secrets and Solutions","Description":"This authoritative guide will help you secure your Linux network - whether you use Linux as a desktop OS, for Internet services, for telecommunications, or for wireless services. Completely rewritten the ISECOM way, Hacking Exposed Linux, Third Editi ...","Image":"http://s.it-ebooks-api.info/13/hacking_exposed_linux_3rd_edition.jpg","isbn":"9780072262575"},{"ID":4031129334,"Title":"Linux Mint System Administrator's","SubTitle":"A practical guide to learn basic concepts, techniques, and tools to become a Linux Mint system administrator","Description":"System administrators are responsible for keeping servers and workstations working properly. They perform actions to get a secure, stable, and robust operating system. In order to do that, system administrators perform actions such as monitoring, acc ...","Image":"http://s.it-ebooks-api.info/14/linux_mint_system_administrators.jpg","isbn":"9781849519601"},{"ID":3348997022,"Title":"The Linux Programming Interface","SubTitle":"A Linux and UNIX System Programming Handbook","Description":"The Linux Programming Interface (TLPI) is the definitive guide to the Linux and UNIX programming interface - the interface employed by nearly every application that runs on a Linux or UNIX system.\n\nIn this authoritative work, Linux programming expe ...","Image":"http://s.it-ebooks-api.info/15/the_linux_programming_interface.jpg","isbn":"9781593272203"},{"ID":2525699380,"Title":"Understanding Linux Network Internals","SubTitle":"Guided Tour to Networking in Linux","Description":"If you've ever wondered how Linux carries out the complicated tasks assigned to it by the IP protocols - or if you just want to learn about modern networking through real-life examples - Understanding Linux Network Internals is for you.\n\nThis book ...","Image":"http://s.it-ebooks-api.info/3/understanding_linux_network_internals.jpg","isbn":"9780596002558"},{"ID":568254925,"Title":"Linux Appliance Design","SubTitle":"A Hands-on Guide to Building Linux Appliances","Description":"Modern appliances are complex machines with processors, operating systems, and application software. While there are books that will tell you how to run Linux on embedded hardware, and books on how to build a Linux application, Linux Appliance Design ...","Image":"http://s.it-ebooks-api.info/15/linux_appliance_design.jpg","isbn":"9781593271404"},{"ID":1596229372,"Title":"Foundations of CentOS Linux","SubTitle":"Enterprise Linux On the Cheap","Description":"You need to maintain clients, servers and networks, while acquiring new skills. Foundations of Cent OS Linux: Enterprise Linux On the Cheap covers a free, unencumbered Linux operating system within the Red Hat lineage, but it does not assume you have ...","Image":"http://s.it-ebooks-api.info/6/foundations_of_centos_linux.jpg","isbn":"9781430219644"},{"ID":331797304,"Title":"Web Penetration Testing with Kali Linux","SubTitle":"A practical guide to implementing penetration testing strategies on websites, web applications, and standard web protocols with Kali Linux","Description":"Kali Linux is built for professional penetration testing and security auditing. It is the next-generation of BackTrack, the most popular open-source penetration toolkit in the world. Readers will learn how to think like real attackers, exploit system ...","Image":"http://s.it-ebooks-api.info/14/web_penetration_testing_with_kali_linux.jpg","isbn":"9781782163169"},{"ID":1478542971,"Title":"Linux Mint Essentials","SubTitle":"A practical guide to Linux Mint for the novice to the professional","Description":"Linux Mint is one of the most popular and proven distributions for beginners and advanced users alike. Out of the box hardware and multimedia support makes Mint your go-to choice for general computing. Its ease of use has transformed it into a celebr ...","Image":"http://s.it-ebooks-api.info/14/linux_mint_essentials.jpg","isbn":"9781782168157"}]
     */

    private String Error;
    private double Time;
    private String Total;
    private int Page;
    private List<Books> Books;

    public void setError(String Error) {
        this.Error = Error;
    }

    public void setTime(double Time) {
        this.Time = Time;
    }

    public void setTotal(String Total) {
        this.Total = Total;
    }

    public void setPage(int Page) {
        this.Page = Page;
    }

    public void setBooks(List<Books> Books) {
        this.Books = Books;
    }

    public String getError() {
        return Error;
    }

    public double getTime() {
        return Time;
    }

    public String getTotal() {
        return Total;
    }

    public int getPage() {
        return Page;
    }

    public List<Books> getBooks() {
        return Books;
    }
}
