package com.tri.itebookwithretrofit.model;

/**
 * Created by tri on 12/20/15.
 */
public class Books {

    /**
     * ID : 1548294494
     * Title : Linux System Administration
     * SubTitle : Solve Real-life Linux Problems Quickly
     * Description : If you're an experienced system administrator looking to acquire Linux skills, or a seasoned Linux user facing a new challenge, Linux System Administration offers practical knowledge for managing a complete range of Linux systems and servers. The boo ...
     * Image : http://s.it-ebooks-api.info/3/linux_system_administration.jpg
     * isbn : 9780596009526
     */
    private long ID;
    private String Title;
    private String SubTitle;
    private String Description;
    private String Image;
    private String isbn;

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public void setTitle(String Title) {
        this.Title = Title;
    }

    public void setSubTitle(String SubTitle) {
        this.SubTitle = SubTitle;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public void setImage(String Image) {
        this.Image = Image;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return Title;
    }

    public String getSubTitle() {
        return SubTitle;
    }

    public String getDescription() {
        return Description;
    }

    public String getImage() {
        return Image;
    }

    public String getIsbn() {
        return isbn;
    }

}
