package com.tri.itebookwithretrofit.retrofit;

import com.tri.itebookwithretrofit.model.Response;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;

/**
 * Created by tri on 12/20/15.
 */
public interface EbookService {
    @GET("search/{query}/page/{halaman}")
    Call<Response> buku(@Path("query") String query, @Path("halaman") int halaman);
}
