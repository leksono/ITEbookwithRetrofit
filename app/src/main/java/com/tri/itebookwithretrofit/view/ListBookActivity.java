package com.tri.itebookwithretrofit.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tri.itebookwithretrofit.R;
import com.tri.itebookwithretrofit.adapter.RecycleAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by tri on 12/21/15.
 */
public class ListBookActivity extends AppCompatActivity {

    @Bind(R.id.recycle_book)
    RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card_book);
        ButterKnife.bind(this);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

        mAdapter = new RecycleAdapter();
        mRecyclerView.setAdapter(mAdapter);

    }
}
